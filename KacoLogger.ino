/*
  Logging serial data (9600 baud, 8 bits, no parity) to the file
  logdata.txt on the SD card using the Adafruit data logger shield.
  When starting, the content of the logdata.txt is dumped to the
  serial connection, so it easily can be read from a PC.
 */

#include <SD.h>
#include <Wire.h>
#include "RTClib.h"

const int RED_LED = 3;
const int GREEN_LED = 4;

RTC_DS1307 RTC;

void setup() {
  Serial.begin(9600);
  Wire.begin();
  
  // CS for SD card
  pinMode(10, OUTPUT);

  pinMode(RED_LED, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);
  
  if (!RTC.isrunning()) {
    Serial.println("RTC is not running");
    RTC.adjust(DateTime(__DATE__, __TIME__));
    digitalWrite(RED_LED, HIGH);
    digitalWrite(GREEN_LED, HIGH);
    return;
  }
  
  // see if the card is present and can be initialized:
  if (!SD.begin(10)) {
    error(250, 250);
    return;
  }
  
  info(500);
  
  File dataFile = SD.open("logdata.txt", FILE_READ);
  if (dataFile) {
    while (dataFile.available()) {
      Serial.write(dataFile.read());
    }
    dataFile.close();

    info(250);
    delay(250);
    info(250);
  }  

  for (;;) {
    if (!Serial.available()) {
      continue;
    }

    dataFile = SD.open("logdata.txt", FILE_WRITE);
    if (!dataFile) {
      error(100, 900);
      return;
    }
  
    digitalWrite(RED_LED, HIGH);
    dataFile.seek(dataFile.size());

    do {
      byte value = Serial.read();
      if (value == 0x0d) {
        digitalWrite(GREEN_LED, HIGH);
        DateTime now = RTC.now();
        digitalWrite(GREEN_LED, LOW);
        dataFile.print(now.year(), DEC);
        dataFile.print('-');
        printBcd2(dataFile, now.month());
        dataFile.print('-');
        printBcd2(dataFile, now.day());
        dataFile.print(' ');
        printBcd2(dataFile, now.hour());
        dataFile.print(':');
        printBcd2(dataFile, now.minute());
      }
      dataFile.write(value);
    }
    while (Serial.available());
    
    dataFile.close();
    digitalWrite(RED_LED, LOW);
  }
}

void loop() {
}

void error(int on, int off) {
  for (;;) {
    digitalWrite(RED_LED, HIGH);
    delay(on);
    digitalWrite(RED_LED, LOW);
    delay(off);
  }
}

void info(int time) {
  digitalWrite(GREEN_LED, HIGH);
  delay(time);
  digitalWrite(GREEN_LED, LOW);
}

void printBcd2(Print& print, byte value) {
  byte upper = value / 10;
  print.print(upper, DEC);
  value -= upper * 10;
  print.print(value, DEC);
}
